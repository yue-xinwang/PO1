// 员工管理系统  ————  客户端
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<sys/types.h>

#define   N    32
#define   LO   0   //登录
#define   CL   1   //打卡
#define   QU   2   //查询
#define   RE   3   //修改
#define   HI   4   //历史
#define   AD   5   //增加
#define   FD   6   //修改
#define   FI   7   //查询
#define   DE   8   //删除

typedef struct{
    int type;       //操作类型
    int ID;         //ID工号
    char name[N];   //姓名
    int age;        //年龄
    char sex[N];    //性别
    char passwd[N]; //密码
    float salary;   //工资
    char dep[N];    //部门
    char data[128]; //数据
}MSG;

//——————————————————————————————函数声明——————————————————————————————
int menu(int sockfd);                  //菜单
int do_login(int sockfd,MSG *msg);     //登录

int do_clock(int sockfd,MSG *msg);     //打卡
int do_query(int sockfd,MSG *msg);     //查询
int do_history(int sockfd,MSG *msg);   //历史
int do_revisepw(int sockfd,MSG *msg);  //修改密码

int do_add(int sockfd,MSG *msg);       //增加
int do_find(int sockfd,MSG *msg);      //查询
int do_revise(int sockfd,MSG *msg);    //修改
int do_dele(int sockfd,MSG *msg);      //删除

//——————————————————————————————主函数——————————————————————————————
int main(int argc, char const *argv[])
{
    int sockfd,n;
    struct sockaddr_in serveraddr;

    if(argc!=3){    //执行传参  地址  端口
        printf("please inpot %s ip & port\n",argv[0]);
        return -1;
    }
    if ((sockfd=socket(AF_INET,SOCK_STREAM,0)) <0)  //创建连接套接字
    {
        printf("socket error\n");
        return -1;
    }
    bzero(&serveraddr,sizeof(serveraddr));  //清0
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_addr.s_addr =inet_addr(argv[1]);
    serveraddr.sin_port = htons(atoi(argv[2]));

    if (connect(sockfd,(struct sockaddr*)&serveraddr,sizeof(serveraddr))<0) //连接
    {
        printf("connect error\n");
        return -1;
    }
    while(1){
        printf("#########  STAFF SYSTEM  #########\n");
        printf("||     1.login      2 exit      ||\n");
        printf("##################################\n");
        printf(">");
        scanf("%d",&n);
        getchar();

        switch(n){
            case 1:
                menu(sockfd);//菜单
                break;
            case 2:
                close(sockfd); //关闭
                printf("---------------------EXIT---------------------\n");
                exit(0);
                break;
            default:
                printf("plaese invalid data\n");
        }
    }
    return 0;
}

//——————————————————————————————功能实现——————————————————————————————
int menu(int sockfd){   //<菜单>
    MSG msg;    //全局通用结构体建立
    int n=0;
    do_login(sockfd,&msg)==1;   //执行登录
    if(msg.type == 10){ //身份验证-用户
        while(1){   //用户-菜单
            printf("\n=========================== WELCOME =========================\n");
            printf("||  1.Clock     2.Query     3.History   4.Revise    5.Exit  ||\n");
            printf("=============================================================\n");
            printf(">>");
            scanf("%d",&n);
            getchar();
            switch(n){
                case 1:
                    do_clock(sockfd,&msg);//打卡
                    break;
                case 2:
                    do_query(sockfd,&msg);//查询
                    break;
                case 3:               
                    do_history(sockfd,&msg);//历史
                    break;
                case 4:               
                    do_revisepw(sockfd,&msg);//修改密码
                    break;
                case 5:
                    return 0;
                default:
                    printf("plaese invalid data\n");
            }
        } 
    }
    else if(msg.type == 11){
        while(1){   //管理员-菜单
            printf("\n===========================* MASTER *========================\n");
            printf("||  1.Add     2.Revise     3.Find     4.Delete    5.Exit   ||\n");
            printf("=============================================================\n");
            printf(">>");
            scanf("%d",&n);
            getchar();
            switch(n){
                case 1:
                    do_add(sockfd,&msg);//增加
                    break;
                case 2:
                    do_revise(sockfd,&msg);//修改
                    break;
                case 3:
                    do_find(sockfd,&msg);//查询
                    break;
                case 4:
                    do_dele(sockfd,&msg);//删除
                    break;
                case 5:
                    return 0;
                default:
                    printf("plaese invalid data\n");
            }
        }
    }
    return 0;
}

int do_login(int sockfd,MSG *msg)   //<登录>
{
    msg->type=LO;
    printf("login>>ID:");//输入姓名
    scanf("%d",&(msg->ID));
    getchar();
    printf("login>>passwd:");//用户密码
    scanf("%s",msg->passwd);
    getchar();
    //发送
    if(send(sockfd,msg,sizeof(MSG),0)<0){
        printf("@ send error.\n");
        return -1;
    }
    if (recv(sockfd,msg,sizeof(MSG),0) <0)  //接收
    {
        printf("recv error\n");
        return -1;
    }
    if (msg->type==10|msg->type==11)  //登录验证成功
    {
        printf("login success !\n");
        return 1;
    }
    else{                             //登录验证失败
        printf("login error !\n");
        return -1;
    }
}

int do_clock(int sockfd,MSG *msg)//<打卡>
{
    msg->type=CL;

    if (send(sockfd,msg,sizeof(MSG),0) <0)  //发送
    {
        printf("send error\n");
        return -1;
    }
    if (recv(sockfd,msg,sizeof(MSG),0) <0)  //接收
    {
        printf("recv error\n");
        return -1;
    }
    if ((msg->type)!=(CL+100)) //打卡验证
    {
        printf("clock error !\n");
        return -1;
    }
    printf("\n---------------  Think you !---------------\n");
    printf("( Hello %s ,this Clock Successfully ! )\n(Time: %s )\n",msg->name,msg->data);
    return 1;
}

int do_query(int sockfd,MSG *msg)//<查询个人信息>
{
    msg->type=QU;
    if (send(sockfd,msg,sizeof(MSG),0) <0)  //发送
    {
        printf("send error\n");
        return -1;
    }
    if (recv(sockfd,msg,sizeof(MSG),0) <0)  //接收 什么也没有做
    {
        printf("recv error\n");
        return -1;
    }
    //打印个人信息
    printf("\n|ID:%d| name:%s| age:%d| sex:%s| passwd:%s| salary:%f| dep:%s|\n"\
    ,msg->ID,msg->name,msg->age,msg->sex,msg->passwd,msg->salary,msg->dep);

    return 0;
}

int do_history(int sockfd,MSG *msg)//<历史>
{
    msg->type=HI;
    if (send(sockfd,msg,sizeof(MSG),0) <0)  //发送
    {
        printf("send error\n");
        return -1;
    }
    while (1)
    {
        if (recv(sockfd,msg,sizeof(MSG),0) <0)  //接收
        {
            printf("recv error\n");
            return -1;
        }
        if(msg->data[0] == '\0')//直到遇见‘\0’才跳出，不然一直打印
        {
            break;
        }
        //打印历史
        printf("%s",msg->data);
    }
    return 0;
}

int do_revisepw(int sockfd,MSG *msg)//<修改密码>
{
    msg->type=RE;
    printf("Old passwd>");
    scanf("%s",msg->passwd);
    getchar();

    printf("New passwd>>");
    scanf("%s",msg->data);  //数据放在data里，到服务器把data赋值给passwd
    getchar();

    if (send(sockfd,msg,sizeof(MSG),0) <0)  //发送
    {
        printf("send error\n");
        return -1;
    }
    if (recv(sockfd,msg,sizeof(MSG),0) <0)  //接收
        {
            printf("recv error\n");
            return -1;
        }
    if(msg->type != RE+100)
    {
        printf("revisepw error\n");
        return -1;
    }
    printf("revisepw success !\n");
    return -0;
}

int do_add(int sockfd,MSG *msg) //<增加>
{
    msg->type=AD;
    printf("### Please enter the following information###\n");
    printf(">    ID:");
    scanf("%d",&(msg->ID));
    printf(">  nmae:");
    scanf("%s",msg->name);
    printf(">   age:");
    scanf("%d",&(msg->age));
    printf(">   sex:");
    scanf("%s",msg->sex);
    printf(">passwd:");
    scanf("%s",msg->passwd);
    printf(">salary:");
    scanf("%f",&(msg->salary));
    printf(">   dep:");
    scanf("%s",msg->dep);
    printf("--------------------OK--------------------\n");

    if (send(sockfd,msg,sizeof(MSG),0) <0)  //发送
    {
        printf("send error\n");
        return -1;
    }
    else{
        printf("add success !\n");
        return 1;
    }
}

int do_revise(int sockfd,MSG *msg)//<修改>
{
    msg->type=FD;
    printf("####### REVISE ########\n");
    printf("Iput revise ID>");
    scanf("%d",&(msg->ID));
    getchar();

    if (send(sockfd,msg,sizeof(MSG),0) <0)  //发送
    {
        printf("send error\n");
        return -1;
    }
    if (recv(sockfd,msg,sizeof(MSG),0) <0)  //接收
    {
        printf("recv error\n");
        return -1;
    }
    printf("\n| ID:%d| name:%s| age:%d| sex:%s| passwd:%s| salary:%f| dep:%s |\n"\
    ,msg->ID,msg->name,msg->age,msg->sex,msg->passwd,msg->salary,msg->dep);

    while(1){
        int n;
        printf("\n################################ Chose Revise ##############################\n");
        printf("###| 0.EXIT | 1.ID | 2.name| 3.age| 4.sex | 5.passwd | 6.salary | 7.dep |###\n");
        printf("############################################################################\n");
        printf("Chose Revise>");//更改选择
        scanf("%d",&n);
        getchar();
            switch (n){
            case 0:
                printf(">EXIT");//退出
                return 0;
                break;
            case 1:
                printf("ID>>");//更改内容
                scanf("%d",&(msg->ID));
                break;
            case 2:
                printf("name>>");//更改内容
                scanf("%s",msg->name);
                break;
            case 3:
                printf("age>>");//更改内容
                scanf("%d",&(msg->age));
                break;
            case 4:
                printf("sex>>");//更改内容
                scanf("%s",msg->sex);
                break;
            case 5:
                printf("passwd>>");//更改内容
                scanf("%s",msg->passwd);
                break;
            case 6:
                printf("salary>>");//更改内容
                scanf("%f",&(msg->salary));
                break;
            case 7:
                printf("dep>>");//更改内容
                scanf("%s",msg->dep);
                break;
            default:
                break;
            }
            if (send(sockfd,msg,sizeof(MSG),0) <0)  //发送
            {
                printf("send error\n");
                return -1;
            }
            if (recv(sockfd,msg,sizeof(MSG),0) <0)  //接收
            {
                printf("recv error\n");
                return -1;
            }
            if (msg->type != RE+100)
            {
               printf("revise error !\n");
               return -1;
            }
            printf("revise success !\n");
            printf("\n| ID:%d | name:%s | age:%d | sex:%s | passwd:%s | salary:%f | dep:%s |\n"\
            ,msg->ID,msg->name,msg->age,msg->sex,msg->passwd,msg->salary,msg->dep);
        }
    return 0;
}

int do_find(int sockfd,MSG *msg)//<查询>
{
    msg->type=FI;
    while(1){
        int n;
        printf("\n############# FIND ############\n");
        printf("### 0.EXIT  1.user  2.clock ###\n");
        printf("###############################\n");
        printf("Chose find>");//更改选择
        scanf("%d",&n);
        getchar();
            switch (n)
            {
            case 0:
                printf(">EXIT");//退出
                return 0;
                break;
            case 1:
                msg->type=FI;
                printf("finf ID>");//查找ID
                scanf("%d",&(msg->ID));
                getchar();
                break;
            case 2:
                msg->type=FI+10;
                printf("find clock ID>");//查找打卡ID
                scanf("%d",&(msg->ID));
                getchar();
                break;
            default:
                break;
            }
        if (send(sockfd,msg,sizeof(MSG),0) <0)  //发送
        {
            printf("send error\n");
            return -1;
        }
        if (recv(sockfd,msg,sizeof(MSG),0) <0)  //接收
        {
            printf("recv error\n");
            return -1;
        }
        if (msg->type==FI+100)
        {
            printf("\n| ID:%d | name:%s | age:%d | sex:%s | passwd:%s | salary:%f | dep:%s |\n"\
        ,msg->ID,msg->name,msg->age,msg->sex,msg->passwd,msg->salary,msg->dep);
        }
        else if (msg->type==FI+110)
        {
            while (1)
            {
                if (recv(sockfd,msg,sizeof(MSG),0) <0)  //接收
                {
                    printf("recv error\n");
                    return -1;
                }
                if(msg->data[0] == '\0')//直到遇见‘\0’才跳出
                {
                    break;
                }
                printf("-%s-",msg->data);//所有打卡信息
            }
        }
        else {
            printf("## find error ! ##\n");
        }
    }
    return 0;
}

int do_dele(int sockfd,MSG *msg)//<删除>
{
    msg->type=DE;
    printf("Delete ID>");
    scanf("%d",&(msg->ID));
    getchar();

    if (send(sockfd,msg,sizeof(MSG),0) <0)  //发送
    {
        printf("send error\n");
        return -1;
    }
    if (recv(sockfd,msg,sizeof(MSG),0) <0)  //接收
    {
        printf("recv error\n");
        return -1;
    }
    if(msg->type!=DE+100){
        printf("delete error !\n");
        return -1;
    }
    printf("delete success !\n");
    return 0;
}

