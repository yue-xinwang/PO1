// 员工管理系统  ————  服务端
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<sys/types.h>
#include<signal.h>
#include<sqlite3.h>
#include<time.h>

#define DATABASE "system.db"
#define   N   32
#define   LO   0   //登录
#define   CL   1   //打卡
#define   QU   2   //查询
#define   RE   3   //修改
#define   HI   4   //历史
#define   AD   5   //增加
#define   FD   6   //修改
#define   FI   7   //查询
#define   DE   8   //删除

typedef struct{
    int type;       //操作类型
    int ID;         //ID工号
    char name[N];   //姓名
    int age;        //年龄
    char sex[N];    //性别
    char passwd[N]; //密码
    float salary;   //工资
    char dep[N];    //部门
    char data[128]; //数据
}MSG;

//——————————————————————————————函数声明——————————————————————————————
int do_client(int acceptfd,sqlite3 *db);             //连接

int do_login(int acceptfd,MSG *msg,sqlite3 *db);     //登录*

int do_clock(int acceptfd,MSG *msg,sqlite3 *db);     //打卡*    用户
int do_query(int acceptfd,MSG *msg,sqlite3 *db);      //查询
int do_history(int acceptfd,MSG *msg,sqlite3 *db);   //历史
int do_revisepw(int acceptfd,MSG *msg,sqlite3 *db);  //修改密码

int do_add(int acceptfd,MSG *msg,sqlite3 *db);       //增加*    管理
int do_find(int acceptfd,MSG *msg,sqlite3 *db);      //查询
int do_revise(int acceptfd,MSG *msg,sqlite3 *db);    //修改
int do_dele(int acceptfd,MSG *msg,sqlite3 *db);      //删除

int get_time(char *data);  //获取时间工具*
int sql_callback(void *arg,int f_ncolumn,char** f_value1,char** f_value2);//历史回调函数工具*

//——————————————————————————————主函数——————————————————————————————
int main(int argc, char const *argv[])
{
   int sockfd,n,acceptfd;
   pid_t pid;
   struct sockaddr_in serveraddr;
   sqlite3 *db;

    if(argc!=3){ //执行参数IP 端口
        printf("please inpot %s ip & port\n",argv[0]);
        return -1;
    }
    if (sqlite3_open(DATABASE,&db) != SQLITE_OK)    //打开数据库
    {
        printf("%s\n",sqlite3_errmsg(db));
        return -1;
    }
    else{
        printf("1-open squlit3 success\n");
    }
    if ((sockfd=socket(AF_INET,SOCK_STREAM,0)) <0)   //创建套接字
    {
        printf("socket error\n");
        return -1;
    }
    bzero(&serveraddr,sizeof(serveraddr));  //内存区域清0
    serveraddr.sin_family = AF_INET;    //填充信息结构体
    serveraddr.sin_addr.s_addr =inet_addr(argv[1]);
    serveraddr.sin_port = htons(atoi(argv[2]));

    if (bind(sockfd,(struct sockaddr *)&serveraddr,sizeof(serveraddr)) < 0) //绑定
    {
        perror("bind error\n");
        return -1;
    }
    if (listen(sockfd,10) < 0)   //监听 最大同时10个
    {
        perror("listen error\n");
        return -1;
    }
    signal(SIGCHLD,SIG_IGN); //处理僵尸等待忽略
    while(1){
        if ((acceptfd=accept(sockfd,NULL,NULL))<0) //连接
        {
            perror("accept error\n");
            return -1;
        }
        printf("2-accept success\n");
        if ((pid=fork())<0)
        {
            perror("fork error\n");
            return -1;
        }
        else if (pid == 0)
        {
            close(sockfd);  //子进程 节约 监听 套接字
            do_client(acceptfd,db);//子进程连接**
        }
        else{
            close(acceptfd);   //父进程 节约 连接 套接字
        }   
    }
    return 0;
}

//——————————————————————————————功能实现——————————————————————————————————
int do_client(int acceptfd,sqlite3 *db){    //<连接>
    MSG msg;
    while (recv(acceptfd,&msg,sizeof(msg),0)>0)     //循环接收
    {
        printf("~type :%d \n",msg.type);    //判断操作进入哪个分支
        switch(msg.type){
            case 0:      //登录*
                do_login(acceptfd,&msg,db);
                break;
            case 1:      //打卡*
                do_clock(acceptfd,&msg,db);
                break;
            case 2:      //查询
                do_query(acceptfd,&msg,db);
                break;
            case 3:      //修改密码
                do_revisepw(acceptfd,&msg,db);
                break;
            case 4:      //历史
                do_history(acceptfd,&msg,db);
                break;
            case 5:      //增加*
                do_add(acceptfd,&msg,db);
                break;
            case 6:      //修改
                do_revise(acceptfd,&msg,db);
                break;
            case 7:       //查询
                do_find(acceptfd,&msg,db);
                break;
            case 8:       //删除 
                do_dele(acceptfd,&msg,db);
                break;
        }
    }
    printf("# exit success! #\n");//断开
    close(acceptfd);//关闭
    exit(0);
    return 0;//子线程退出
}

int do_login(int acceptfd,MSG *msg,sqlite3 *db)     //<登录>
{
    char sql[512]={0};
    char *errmsg;
    int nrow,ncloumn;
    char **resultp;
    int i,j;

    //登录依据工号和密码
    sprintf(sql,"select *from uer where ID ='%d' and passwd ='%s'",msg->ID,msg->passwd);

    if (sqlite3_get_table(db,sql,&resultp,&nrow,&ncloumn,&errmsg)!=SQLITE_OK)
    {
        printf("%s\n",errmsg);  //git_table失败
        return -1;
    }
    else{
        printf("Get_table success \n");//代表get_table语句执行成功
    }
    if(nrow == 0)
    {
        msg->type=-1; //错误
        printf("login error !\n");
    }
    if(nrow != 0)  //查询成功
    {
        if ((msg->ID) >= 10000)  //注册的员工按工号区分身份
        {
            msg->type=11;//管理员
            printf("admin login success \n");
        }
        else
        {
            msg->type=10; //员工
            printf("user login success \n");
        }
        //存入信息到结构体##
        msg->ID=atoi(resultp[ncloumn+0]);
        strcpy(msg->name,resultp[ncloumn+1]);
        msg->age=atoi(resultp[ncloumn+2]);
        strcpy(msg->sex,resultp[ncloumn+3]);
        strcpy(msg->passwd,resultp[ncloumn+4]);
        msg->salary=(float)atoi(resultp[ncloumn+5]);
        strcpy(msg->dep,resultp[ncloumn+6]);

        sqlite3_free_table(resultp);//查完释放

        //打印个人信息---测试成功！
    printf("\n|ID:%d| name:%s| age:%d| sex:%s| passwd:%s| salary:%f| dep:%s|\n"\
    ,msg->ID,msg->name,msg->age,msg->sex,msg->passwd,msg->salary,msg->dep);
    }
    if(send(acceptfd,msg,sizeof(MSG),0 )<0) //发送
    {
        printf("login error\n");
        return -1;
    }
    return 0;
}

//用户——————————————————————————————————————————————————————————————
int do_clock(int acceptfd,MSG *msg,sqlite3 *db)     //<打卡>
{
    char sql[512]={0};
    char *errmsg;
    int nrow,ncloumn;
    char **resultp;

    get_time(msg->data);//获取系统时间到data 

    sprintf(sql,"insert into record values(%d,'%s','%s','%s')"\
    ,msg->ID,msg->name,msg->data,"OK");//插入表2信息

    if (sqlite3_exec(db,sql,NULL,NULL,&errmsg) !=SQLITE_OK)//记录插入到数据库
    {
        printf("%s\n",errmsg);
        return -1;
    }
    msg->type=CL+100;

    if(send(acceptfd,msg,sizeof(MSG),0 )<0){
        printf("send error\n");
        return -1;
    }
    
    return 0;
}


int do_query(int acceptfd,MSG *msg,sqlite3 *db)      //<查询信息>
{
    msg->type=QU+100;//标识成功  然后什么也不做，原来的msg里就有用户信息

    if(send(acceptfd,msg,sizeof(MSG),0 )<0)
    {
        printf("send error\n");
        return -1;
    }
    printf("query error\n");
}


int do_history(int acceptfd,MSG *msg,sqlite3 *db)   //历史(打卡历史)
{
    char sql[128]={};
    char *errmsg;

    //msg->type=HI+100;//需要从别的函数调用，不需要验证

    sprintf(sql,"select *from record where ID =%d",msg->ID);//找某一个人的记录

    //查询数据库
    if (sqlite3_exec(db,sql,sql_callback,(void *)&acceptfd,&errmsg) != SQLITE_OK) 
    {
        printf("%s\n",errmsg);
    }
    //所有的记录查询完毕，补上一个 '\0'给客户端   标识发送结束
    msg->data[0]='\0';
    if(send(acceptfd,msg,sizeof(MSG),0 )<0){
        printf("send error\n");
        return -1;
    }
}

int do_revisepw(int acceptfd,MSG *msg,sqlite3 *db)  //修改密码
{
    char sql[512]={0};
    char *errmsg;
    int nrow,ncloumn;
    char **resultp;

    //登录依据工号改密码
    //有时间加一下原密码效验######################
    sprintf(sql,"update uer set passwd='%s' where ID =%d ",msg->data,msg->ID);

    if (sqlite3_exec(db,sql,NULL,NULL,&errmsg) !=SQLITE_OK)//记录插入到数据库
    {
        printf("%s\n",errmsg);
        return -1;
    }
    msg->type=RE+100;

    if(send(acceptfd,msg,sizeof(MSG),0 )<0){
        printf("send error\n");
        return -1;
    }
    return 0;
}

//管理————————————————————————————————————————————————————————————
int do_add(int acceptfd,MSG *msg,sqlite3 *db)       //<增加>
{
    char *errmsg;
    char sql[512]={0};//定义用来放数据库指令

    //增加数据库数据
    sprintf(sql,"insert into uer values(%d,'%s',%d,'%s','%s',%f,'%s');"\
    ,msg->ID,msg->name,msg->age,msg->sex,msg->passwd,msg->salary,msg->dep);
    printf("%s \n",sql);//测试打印一下

    if (sqlite3_exec(db,sql,NULL,NULL,&errmsg) != SQLITE_OK )
    {
        printf("%s\n",errmsg);
    }
    else{
        printf("add register ok \n");
    }
    if(send(acceptfd,msg,sizeof(MSG),0 )<0){
        printf("add error\n");
        return -1;
    }
    return 0;
}

int do_find(int acceptfd,MSG *msg,sqlite3 *db)      //查询
{
    char sql[512]={0};
    char *errmsg;
    int nrow,ncloumn;
    char **resultp;

    if (msg->type==FI)
    {
        //依据工号查询
        sprintf(sql,"select *from uer where ID ='%d' ",msg->ID);

        if (sqlite3_get_table(db,sql,&resultp,&nrow,&ncloumn,&errmsg)!=SQLITE_OK)
        {
            printf("%s\n",errmsg);  //git_table失败
            return -1;
        }
        else{
            printf("Get_table success \n");//代表get_table语句执行成功
        }
        if(nrow == 0)
        {
            msg->type=-1; //错误
            printf("find error !\n");
        }
        if(nrow != 0)  //查询成功
        {
            msg->type=FI+100;

            //存入信息到结构体##
            msg->ID=atoi(resultp[ncloumn+0]);
            strcpy(msg->name,resultp[ncloumn+1]);
            msg->age=atoi(resultp[ncloumn+2]);
            strcpy(msg->sex,resultp[ncloumn+3]);
            strcpy(msg->passwd,resultp[ncloumn+4]);
            msg->salary=(float)atoi(resultp[ncloumn+5]);
            strcpy(msg->dep,resultp[ncloumn+6]);

            sqlite3_free_table(resultp);//查完释放

            //打印个人信息---测试成功！
            printf("\n|ID:%d| name:%s| age:%d| sex:%s| passwd:%s| salary:%f| dep:%s|\n"\
            ,msg->ID,msg->name,msg->age,msg->sex,msg->passwd,msg->salary,msg->dep);
        }   

        if(send(acceptfd,msg,sizeof(MSG),0 )<0)//查完发送
        {
            printf("send error\n");
            return -1;
        }
    }
    else if (msg->type==FI+10)
    {
        if (recv(acceptfd,msg,sizeof(MSG),0) <0)  //<接收>
        {
        printf("recv error\n");
        return -1;
        }
        msg->type=FI+110;
        do_history(acceptfd,msg,db);//打卡历史自带发送****************************************************************************************
    }
    return 0;
}


int do_revise(int acceptfd,MSG *msg,sqlite3 *db)    //修改
{
   char sql[512]={0};
    char *errmsg;
    int nrow,ncloumn;
    char **resultp;
    //依据工号查询
        sprintf(sql,"select *from uer where ID ='%d' ",msg->ID);

        if (sqlite3_get_table(db,sql,&resultp,&nrow,&ncloumn,&errmsg)!=SQLITE_OK)
        {
            printf("%s\n",errmsg);  //git_table失败
            return -1;
        }
        else{
            printf("Get_table success \n");//代表get_table语句执行成功
        }
        if(nrow == 0)
        {
            msg->type=-1; //错误
            printf("revise1 error !\n");
        }
        if(nrow != 0)  //查询成功
        {
            //存入信息到结构体##
            msg->ID=atoi(resultp[ncloumn+0]);
            strcpy(msg->name,resultp[ncloumn+1]);
            msg->age=atoi(resultp[ncloumn+2]);
            strcpy(msg->sex,resultp[ncloumn+3]);
            strcpy(msg->passwd,resultp[ncloumn+4]);
            msg->salary=(float)atoi(resultp[ncloumn+5]);
            strcpy(msg->dep,resultp[ncloumn+6]);

            sqlite3_free_table(resultp);//查完释放
        }
    //打印个人信息---测试成功！
    printf("\n|ID:%d| name:%s| age:%d| sex:%s| passwd:%s| salary:%f| dep:%s|\n"\
    ,msg->ID,msg->name,msg->age,msg->sex,msg->passwd,msg->salary,msg->dep);

    if (send(acceptfd,msg,sizeof(MSG),0) <0)  //<发送>
    {
        printf("send error\n");
        return -1;
    }
    if (recv(acceptfd,msg,sizeof(MSG),0) <0)  //<接收>
    {
        printf("recv error\n");
        return -1;
    }
    //依据工号改信息
    sprintf(sql,"update uer set name='%s',age=%d,sex='%s',passwd='%s',salary=%f,dep='%s' where ID =%d"\
    ,msg->name,msg->age,msg->sex,msg->passwd,msg->salary,msg->dep, msg->ID);

    if (sqlite3_exec(db,sql,NULL,NULL,&errmsg) !=SQLITE_OK) //<记录插入到数据库》
    {
        printf("%s\n",errmsg);
        return -1;
    }
    msg->type=RE+100;

    if(send(acceptfd,msg,sizeof(MSG),0) <0){
        printf("send error\n");
        return -1;
    }
    return 0;
}

int do_dele(int acceptfd,MSG *msg,sqlite3 *db)   //<删除>
{
    char sql[128] = {};
    char *errmsg;

    sprintf(sql, "delete from uer where id =%d",msg->ID);

    if (sqlite3_exec(db,sql, NULL, NULL, &errmsg) != SQLITE_OK) 
    {
        printf("%s\n", errmsg);
    } 
 
    msg->type=DE+100;
    printf("delete success\n");

    if(send(acceptfd,msg,sizeof(MSG),0) <0)
    {
        printf("send error\n");
        return -1;
    }
    return 0;
}

int get_time(char *data)    //<获取时间工具>
{
    time_t t;
    struct tm *tp;

    time(&t);//获取时间
    tp=localtime(&t);//格式转化
    //搬运格式：年+1900-月+1-日  时：分：秒
    sprintf(data,"%d-%d-%d %d:%d:%d",\
    tp->tm_year+1900,tp->tm_mon+1,tp->tm_mday,\
    tp->tm_hour,tp->tm_min,tp->tm_sec);
    
    return 0;
}

int sql_callback(void *arg,int f_ncolumn,char** f_value1,char** f_value2){  //<历史回调函数工具>
    //历史记录表内容：表名record , 字段ID， 字段name , 字段data , 字段clock  
    //f_ncolumn记录有多少列，可以用来判断内部有几条记录
    //value1是信息数组 如  1000  admin   2022.2.2 22:22:22 ok
    //value2是字段名数组 如  ID name data clock
    int acceptfd;
    MSG msg;

    acceptfd = *((int *)arg);   //将回调的返回给连接套接字
    sprintf(msg.data,"| %s | %s | %s | %s |\n",f_value1[0],f_value1[1],f_value1[2],f_value1[3]);//value1是信息数组，value2是字段名数组
    send(acceptfd,&msg,sizeof(MSG),0);//每次查询结果都给对方传过去- 列表

    return 0;
}
